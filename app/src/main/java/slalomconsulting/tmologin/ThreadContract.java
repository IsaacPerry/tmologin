package slalomconsulting.tmologin;

import android.provider.BaseColumns;

/**
 * Created by isaac.charny on 8/31/16.
 */
interface ThreadContract {
    String TABLE_NAME = "thread";
    String SQL_CREATE_TABLE = "create table " +
            TABLE_NAME + " ( " +
            Cols._ID + " integer primary key not null, " +
            Cols.COLLEAGUE_GUID + " text unique not null); ";
    String SQL_DROP_TABLE = "drop table if exists " + TABLE_NAME;


    interface Cols extends BaseColumns {
        String COLLEAGUE_GUID = "colleague_guid";
    }
}