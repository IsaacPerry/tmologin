package slalomconsulting.tmologin;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefStorage {

    // TODO remove uses of ANDROID_COOKIE_HEADER.  Use UserPrefs.cookie instead
    public static final String ANDROID_COOKIE_HEADER = "SPOCookieHeader";

    public static String getPreference(Context context, String key) {
        SharedPreferences prefs = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
        return prefs.getString(key, "");
    }

    public static void savePreference(Context context, String key, String value) {
        SharedPreferences prefs = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static void deleteAll(Context context) {
        SharedPrefStorage.savePreference(context, SharedPrefStorage.ANDROID_COOKIE_HEADER, "");
    }
}
