package slalomconsulting.tmologin;

import android.content.Context;

import com.loopj.android.http.SyncHttpClient;

import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicHeader;
import cz.msebera.android.httpclient.protocol.HTTP;

abstract class SpoUtil {
    private SpoUtil() {
        // private constructor to prevent instantiation
    }

    static final String CHARSET = "UTF-8";
    static final String CONTENT_TYPE = "application/json;odata=verbose;";

    // This empty entity is required for some Sharepoint requests because Sharepoint
    static final StringEntity EMPTY_ENTITY = getStringEntity();

    private static StringEntity getStringEntity() {
        StringEntity entity = new StringEntity("", CHARSET);
        entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, CONTENT_TYPE));
        return entity;
    }

    static SyncHttpClient getHttpClient(Context context) {
        SyncHttpClient client = new SyncHttpClient();
        client.setTimeout(10 * 1000);  // 10 seconds
        client.addHeader("Accept", CONTENT_TYPE);
        client.addHeader("Cookie", UserPrefs.getInstance(context).getCookie());
        return client;
    }
}
