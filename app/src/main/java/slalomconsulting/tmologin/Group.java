package slalomconsulting.tmologin;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by isaac.charny on 8/17/16.
 */
public class Group implements Parcelable {
    public enum Visibility {
        PUBLIC("Public"), PRIVATE("Private");

        public final String dbVal;

        Visibility(String dbVal) {
            this.dbVal = dbVal;
        }

        static Visibility fromDbVal(String dbVal) {
            for (Visibility visibility : values()) {
                if (visibility.dbVal.equalsIgnoreCase(dbVal)) {
                    return visibility;
                }
            }
            throw new IllegalArgumentException("No Visibility for dbVal=" + dbVal);
        }
    }

    public enum Type {
        NORMAL(0), RECOMMENDED(1), FAVORITE(2);

        public final int dbVal;

        Type(int dbVal) {
            this.dbVal = dbVal;
        }

        static Type fromDbVal(int dbVal) {
            for (Type type : values()) {
                if (type.dbVal == dbVal) {
                    return type;
                }
            }
            throw new IllegalArgumentException("No Type for dbVal=" + dbVal);
        }
    }

    private Long id;  // from database
    private String groupId;  // from service
    private String title;
    private String path;
    private Visibility visibility;
    private boolean subscribed;
    private Type type = Type.NORMAL;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Visibility getVisibility() {
        return visibility;
    }

    public void setVisibility(Visibility visibility) {
        this.visibility = visibility;
    }

    public String getVisibilityString() {
        return visibility.dbVal.toLowerCase();
    }

    public void setVisibility(String visibilityString) {
        this.visibility = Visibility.fromDbVal(visibilityString);
    }

    public boolean isSubscribed() {
        return subscribed;
    }

    public void setSubscribed(boolean subscribed) {
        this.subscribed = subscribed;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setType(int dbVal) {
        // force enum validation
        this.type = Type.fromDbVal(dbVal);
    }

    public Long getDbId() {
        return getId();
    }

    public static final Creator<Group> CREATOR = new Creator<Group>() {
        @Override
        public Group createFromParcel(Parcel in) {
            Group group = new Group();
            group.fromParcel(in);
            return group;
        }

        @Override
        public Group[] newArray(int size) {
            return new Group[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    void fromParcel(Parcel in) {
        type = Type.fromDbVal(in.readInt());
        id = in.readLong();
        groupId = in.readString();
        title = in.readString();
        path = in.readString();
        visibility = Visibility.fromDbVal(in.readString());
        subscribed = in.readInt() == 1;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeInt(type.dbVal);
        parcel.writeLong(id);
        parcel.writeString(groupId);
        parcel.writeString(title);
        parcel.writeString(path);
        parcel.writeString(visibility.dbVal);
        parcel.writeInt(subscribed ? 1 : 0);
    }


}
