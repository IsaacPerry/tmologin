package slalomconsulting.tmologin;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by joeyt on 7/25/16.
 */
public class DbHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "tnation";
    private static final int DB_VERSION = 25;
    private static DbHelper instance = null;

    static final int SQLITE_FALSE = 0;
    static final int SQLITE_TRUE = 1;

    private DbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    public static synchronized DbHelper getInstance(Context c) {
        if (instance == null) {
            instance = new DbHelper(c.getApplicationContext());
        }
        return instance;
    }

    @Override
    public void onConfigure(SQLiteDatabase db) {
        db.setForeignKeyConstraintsEnabled(true);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(ChatContract.SQL_CREATE_TABLE);
        db.execSQL(ColleagueContract.SQL_CREATE_TABLE);
        db.execSQL(GroupContract.SQL_CREATE_TABLE);
        db.execSQL(TopicContract.SQL_CREATE_TABLE);
        db.execSQL(ThreadContract.SQL_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // should probably do a better upgrade path in the future
        if (oldVersion == 24 && newVersion == 25) {
            db.execSQL(TopicContract.SQL_DROP_TABLE);
            db.execSQL(TopicContract.SQL_CREATE_TABLE);
        } else {
            resetDatabase(db);
        }
    }

    private void resetDatabase(SQLiteDatabase db) {
        db.execSQL(ChatContract.SQL_DROP_TABLE);
        db.execSQL(ColleagueContract.SQL_DROP_TABLE);
        db.execSQL(GroupContract.SQL_DROP_TABLE);
        db.execSQL(TopicContract.SQL_DROP_TABLE);
        db.execSQL(ThreadContract.SQL_DROP_TABLE);
        onCreate(db);
    }

    public void resetDatabase() {
        SQLiteDatabase db = getWritableDatabase();
        resetDatabase(db);
        db.close();
    }

    static int toCol(boolean value) {
        return value ? SQLITE_TRUE : SQLITE_FALSE;
    }

    static boolean boolFromCol(int col) {
        return col == SQLITE_TRUE;
    }
}
