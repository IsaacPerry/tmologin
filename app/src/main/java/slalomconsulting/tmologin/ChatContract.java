package slalomconsulting.tmologin;

import android.provider.BaseColumns;

/**
 * Created by isaac.charny on 8/9/16.
 */
interface ChatContract {
    String TABLE_NAME = "chats";
    String SQL_CREATE_TABLE = "create table " +
            TABLE_NAME + " ( " +
            Cols._ID + " integer primary key not null, " +
            Cols.CHAT_GUID + " text unique not null, " +
            Cols.CHAT_TEXT + " text not null, " +
            Cols.POST_DATE_MILLIS + " numeric not null, " +
            Cols.I_AM_SENDER + " integer not null, " +
            Cols.THREAD_ID + " integer not null, " +
            String.format("foreign key(%s) references %s(%s) on delete cascade", Cols.THREAD_ID, ThreadContract.TABLE_NAME, ThreadContract.Cols._ID) + ");";
    String SQL_DROP_TABLE = "drop table if exists " + TABLE_NAME;


    interface Cols extends BaseColumns {
        String CHAT_GUID = "chat_guid";
        String CHAT_TEXT = "chat_text";
        String POST_DATE_MILLIS = "post_date";
        String I_AM_SENDER = "i_am_sender";
        String THREAD_ID = "thread_id";
    }
}