package slalomconsulting.tmologin;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

/**
 * A pojo for colleagues saved in sqlite.
 */
public class Colleague implements Parcelable {

    public enum Type {
        ME(-1), NORMAL(0), RECOMMENDED(1), FAVORITE(2);

        public final int dbVal;

        Type(int dbVal) {
            this.dbVal = dbVal;
        }

        static Type fromDbVal(int dbVal) {
            for (Type type : values()) {
                if (type.dbVal == dbVal) {
                    return type;
                }
            }
            throw new IllegalArgumentException("No Type for dbVal=" + dbVal);
        }
    }

    // will not be null if retrieved from sqlite
    private Long colleagueId;

    private String workEmail;
    private String workPhone;
    private String cellPhone;
    private String userProfileGuid;
    private String office;
    private String firstName;
    private String lastName;
    private String pictureUrl;
    private String accountName;
    private String jobTitle;
    private String officeNumber;
    private Type type = Type.NORMAL;

    private ChatMessage lastChat;

    public ChatMessage getLastChat() {
        return lastChat;
    }

    public void setLastChat(ChatMessage lastChat) {
        this.lastChat = lastChat;
    }

    public Long getColleagueId() {
        return colleagueId;
    }

    public void setColleagueId(Long colleagueId) {
        this.colleagueId = colleagueId;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    public String getUserProfileGuid() {
        return userProfileGuid;
    }

    public void setUserProfileGuid(String userProfileGuid) {
        this.userProfileGuid = userProfileGuid;
    }

    public String getWorkEmail() {
        return workEmail;
    }

    public void setWorkEmail(String workEmail) {
        this.workEmail = workEmail;
    }

    public String getWorkPhone() {
        return workPhone;
    }

    public void setWorkPhone(String workPhone) {
        this.workPhone = workPhone;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setType(int dbVal) {
        this.type = Type.fromDbVal(dbVal);
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getOfficeNumber() {
        return officeNumber;
    }

    public void setOfficeNumber(String officeNumber) {
        this.officeNumber = officeNumber;
    }

    public String getDisplayName() {
        if (TextUtils.isEmpty(lastName)) {
            return firstName;
        } else {
            return String.format("%s %s", firstName, lastName);
        }
    }

    public static final Creator<Colleague> CREATOR = new Creator<Colleague>() {
        @Override
        public Colleague createFromParcel(Parcel in) {
            Colleague colleague = new Colleague();
            colleague.fromParcel(in);
            return colleague;
        }

        @Override
        public Colleague[] newArray(int size) {
            return new Colleague[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    void fromParcel(Parcel in) {
        type = Type.fromDbVal(in.readInt());
        colleagueId = in.readLong();
        workEmail = in.readString();
        workPhone = in.readString();
        cellPhone = in.readString();
        userProfileGuid = in.readString();
        office = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        pictureUrl = in.readString();
        accountName = in.readString();
        jobTitle = in.readString();
        officeNumber = in.readString();
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeInt(type.dbVal);
        long cid = 0;
        if (colleagueId != null) {
            cid = colleagueId;
        }
        parcel.writeLong(cid);
        parcel.writeString(workEmail);
        parcel.writeString(workPhone);
        parcel.writeString(cellPhone);
        parcel.writeString(userProfileGuid);
        parcel.writeString(office);
        parcel.writeString(firstName);
        parcel.writeString(lastName);
        parcel.writeString(pictureUrl);
        parcel.writeString(accountName);
        parcel.writeString(jobTitle);
        parcel.writeString(officeNumber);
    }
}