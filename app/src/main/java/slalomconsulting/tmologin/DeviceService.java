package slalomconsulting.tmologin;

import android.content.Context;
import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.atomic.AtomicBoolean;

import cz.msebera.android.httpclient.Header;

/**
 * Service class for device operations.
 */
public abstract class DeviceService {
    private DeviceService() {
        // private constructor to prevent instantiation
    }

    static final String TAG = DeviceService.class.getSimpleName();

    public static boolean createDevice(Context context, String deviceToken) {
        if (deviceToken == null) {
            Log.w(TAG, "createDevice: deviceToken == null");
            return false;
        }

        String restUrl = context.getString(R.string.config_azure_url) + "/api/devices";
        SyncHttpClient client = AzureUtil.getHttpClient(context);

        RequestParams params = new RequestParams();
        params.put("Token", deviceToken);
        params.put("Platform", "Android");
        params.put("NotificationService", "Firebase");

        final AtomicBoolean postSucceeded = new AtomicBoolean(false);
        client.post(context, restUrl, params, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Log.e(TAG, "createDevice.onFailure sc=" + statusCode + " errRes=" + errorResponse, throwable);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                postSucceeded.set(true);
            }
        });
        return postSucceeded.get();
    }

}
