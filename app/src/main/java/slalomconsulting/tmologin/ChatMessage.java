package slalomconsulting.tmologin;

/**
 * Created by isaac.charny on 8/22/16.
 */
public class ChatMessage {
    private final String senderGuid;
    private final String chatText;
    private final long postDateMillis;
    private final String chatGuid;
    private final String recipientGuid;

    public ChatMessage(String chatGuid, String senderGuid, String recipientGuid, String chatText, long postDateMillis) {
        this.senderGuid = senderGuid;
        this.chatText = chatText;
        this.postDateMillis = postDateMillis;
        this.chatGuid = chatGuid;
        this.recipientGuid = recipientGuid;
    }

    public String getSenderGuid() {
        return senderGuid;
    }

    public String getChatText() {
        return chatText;
    }

    public long getPostDateMillis() {
        return postDateMillis;
    }

    public String getChatGuid() {
        return chatGuid;
    }

    public String getRecipientGuid() {
        return recipientGuid;
    }

    public static String otherGuid(ChatMessage chat, String userGuid) {
        if (userGuid.equals(chat.getRecipientGuid().toUpperCase())) {
            return chat.getSenderGuid();
        }
        return chat.getRecipientGuid();
    }
}
