package slalomconsulting.tmologin;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Data layer Favorites.  Use the singleton here to do query/add/remove favorites, but do not call from the UI thread.
 */
public class ColleagueDao {
    private static final String TAG = ColleagueDao.class.getSimpleName();
    private static ColleagueDao instance = null;

    private final DbHelper helper;

    private ColleagueDao(Context context) {
        helper = DbHelper.getInstance(context);
    }

    public static synchronized ColleagueDao getInstance(Context c) {
        if (instance == null) {
            instance = new ColleagueDao(c.getApplicationContext());
        }
        return instance;
    }


    @NonNull
    private static ContentValues toContentValues(Colleague person) {
        ContentValues values = new ContentValues();
        values.put(ColleagueContract.Cols.USER_PROFILE_GUID, person.getUserProfileGuid().toUpperCase());
        values.put(ColleagueContract.Cols.WORK_EMAIL, person.getWorkEmail());
        values.put(ColleagueContract.Cols.WORK_PHONE, person.getWorkPhone());
        values.put(ColleagueContract.Cols.CELL_PHONE, person.getCellPhone());
        values.put(ColleagueContract.Cols.OFFICE, person.getOffice());
        values.put(ColleagueContract.Cols.FIRST_NAME, person.getFirstName());
        values.put(ColleagueContract.Cols.LAST_NAME, person.getLastName());
        values.put(ColleagueContract.Cols.PICTURE_URL, person.getPictureUrl());
        values.put(ColleagueContract.Cols.ACCOUNT_NAME, person.getAccountName());
        values.put(ColleagueContract.Cols.TYPE_ENUM, person.getType().dbVal);
        values.put(ColleagueContract.Cols.JOB_TITLE, person.getJobTitle());
        values.put(ColleagueContract.Cols.OFFICE_NUMBER, person.getOfficeNumber());
        return values;
    }

    public void replaceMe(Colleague me) {
        me.setType(Colleague.Type.ME);
        update(me);
    }

    public void update(Colleague colleague) {
        ContentValues values = toContentValues(colleague);
        long rowId = helper.getWritableDatabase().replaceOrThrow(ColleagueContract.TABLE_NAME, null, values);
        if (rowId == -1) {
            throw new RuntimeException("Error replacing colleague, database returned -1");
        }
        colleague.setColleagueId(rowId);
    }

}
