package slalomconsulting.tmologin;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class XmlParser {

    private Document _xmlDoc;

    public XmlParser(String xmlString) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(xmlString));

        _xmlDoc = db.parse(is);
    }

    public String getElementStringByTagName(String tagName) throws IOException, SAXException, ParserConfigurationException {
        NodeList nodes = _xmlDoc.getElementsByTagName(tagName);
        String val = "";
        if(nodes.getLength() > 0) {
            Element element = (Element) nodes.item(0);
            val = element.getFirstChild().getNodeValue();
        }
        return val;
    }

    public NodeList getElementChildrenByTagName(String tagName) throws IOException, SAXException, ParserConfigurationException {
        NodeList nodes = _xmlDoc.getElementsByTagName(tagName);
        NodeList children = null;
        if(nodes.getLength() > 0) {
            Element element = (Element) nodes.item(0);
            children = element.getChildNodes();
        }
        return children;
    }
}
