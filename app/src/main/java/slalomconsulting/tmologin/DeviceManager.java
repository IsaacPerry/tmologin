package slalomconsulting.tmologin;

import android.content.Context;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;

import java.util.concurrent.Callable;

import rx.Observable;

/**
 * Created by joeyt on 9/12/16.
 */
public abstract class DeviceManager {
    private DeviceManager() {
        // private constructor to prevent instantiation
    }

    static final String TAG = DeviceManager.class.getSimpleName();

    public static Observable<Boolean> updateUserDevice(final Context context) {
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                String deviceToken = FirebaseInstanceId.getInstance().getToken();
                Log.v(TAG, "Updating firebase token=" + deviceToken);
                return DeviceService.createDevice(context, deviceToken);
            }
        });
    }
}
