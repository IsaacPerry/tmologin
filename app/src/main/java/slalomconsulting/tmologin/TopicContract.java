package slalomconsulting.tmologin;

import android.provider.BaseColumns;

/**
 * Created by isaac.charny on 8/19/16.
 */
interface TopicContract {
    String TABLE_NAME = "topic";
    String SQL_CREATE_TABLE = "create table " +
            TABLE_NAME + " ( " +
            Cols._ID + " integer primary key not null, " +
            Cols.TOPIC_ID + " text not null, " +
            Cols.TITLE + " text, " +
            Cols.GROUP_DB_ID + " integer not null, " +
            Cols.NUMBER_OF_CHATS + " integer not null default 0, " +
            Cols.LAST_CHAT_DATE_MILLIS + " integer not null, " +
            Cols.SUBSCRIBED + " integer not null default " + DbHelper.SQLITE_FALSE + ", " +
            Cols.CATEGORY + " text, " +
            Cols.BODY + " text, " +
            Cols.IMAGE_URL_FULL + " text, " +
            Cols.IMAGE_URL_OPT + " text, " +
            Cols.LIKES + " integer not null default 0, " +
            Cols.LIKED + " integer not null default " + DbHelper.SQLITE_FALSE + ", " +
            Cols.CREATOR + " text, " +
            Cols.CREATED_MILLIS + " integer not null, " +
            Cols.VIDEO_URL + " text, " +
            Cols.TYPE + " text not null, " +
            Cols.POPULARITY + " real not null default 0, " +
            "unique (" + Cols.GROUP_DB_ID + ", " + Cols.TOPIC_ID + ", " + Cols.TYPE + ") " +
            "foreign key(" + Cols.GROUP_DB_ID + ") references " + GroupContract.TABLE_NAME +
            "(" + GroupContract.Cols._ID + ") on delete cascade " +
            ");";
    String SQL_DROP_TABLE = "drop table if exists " + TABLE_NAME;


    interface Cols extends BaseColumns {
        String TOPIC_ID = "topic_id";
        String TITLE = "title";
        String NUMBER_OF_CHATS = "num_chats";
        String LAST_CHAT_DATE_MILLIS = "last_chat_date";
        String GROUP_DB_ID = "group_db_id";
        String SUBSCRIBED = "subscribed";
        String CATEGORY = "category";
        String BODY = "body";
        String IMAGE_URL_FULL = "image_url_full";
        String IMAGE_URL_OPT = "image_url_opt";
        String LIKES = "likes";
        String LIKED = "liked";
        String CREATOR = "creator";
        String CREATED_MILLIS = "created_date";
        String TYPE = "type";
        String POPULARITY = "popularity";
        String VIDEO_URL = "video_url";
    }
}

