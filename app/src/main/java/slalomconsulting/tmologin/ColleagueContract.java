package slalomconsulting.tmologin;

import android.provider.BaseColumns;

/**
 * Created by joeyt on 7/25/16.
 */
interface ColleagueContract {
    String TABLE_NAME = "colleague";
    String SQL_CREATE_TABLE = "create table " +
            TABLE_NAME + " ( " +
            Cols._ID + " integer primary key not null, " +
            Cols.USER_PROFILE_GUID + " text unique not null, " +
            Cols.WORK_EMAIL + " text, " +
            Cols.WORK_PHONE + " text, " +
            Cols.CELL_PHONE + " text, " +
            Cols.OFFICE + " text, " +
            Cols.FIRST_NAME + " text, " +
            Cols.LAST_NAME + " text, " +
            Cols.PICTURE_URL + " text, " +
            Cols.ACCOUNT_NAME + " text unique not null, " +
            Cols.TYPE_ENUM + " integer not null default 0, " +
            Cols.JOB_TITLE + " text, " +
            Cols.OFFICE_NUMBER + " text); ";
    String SQL_DROP_TABLE = "drop table if exists " + TABLE_NAME;


    interface Cols extends BaseColumns {
        String USER_PROFILE_GUID = "user_profile_guid";
        String WORK_EMAIL = "work_email";
        String WORK_PHONE = "work_phone";
        String CELL_PHONE = "cell_phone";
        String OFFICE = "office";
        String FIRST_NAME = "first_name";
        String LAST_NAME = "last_name";
        String PICTURE_URL = "picture_url";
        String ACCOUNT_NAME = "account_name";
        String TYPE_ENUM = "type_enum";
        String JOB_TITLE = "job_title";
        String OFFICE_NUMBER = "office_number";
    }
}
