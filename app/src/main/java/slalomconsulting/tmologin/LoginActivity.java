package slalomconsulting.tmologin;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.HttpAuthHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.exceptions.OnErrorNotImplementedException;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;


public class LoginActivity extends AppCompatActivity {
    static final String TAG = LoginActivity.class.getSimpleName();

    private final CompositeSubscription subscription = new CompositeSubscription();

    ProgressBar progressBar;
    private WebView webView;

    private LinearLayout loginScreen;
    private LinearLayout loadingScreen;

    @SuppressLint("AndroidLintSetJavaScriptEnabled")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        String title = getResources().getString(R.string.title_login);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(title);

        int color = ContextCompat.getColor(this, R.color.tmobile_magenta);

        loginScreen = (LinearLayout) findViewById(R.id.loginScreen);
        loadingScreen = (LinearLayout) findViewById(R.id.loadingScreen);

        ((ProgressBar)loadingScreen.findViewById(R.id.loadingProgressBar)).getIndeterminateDrawable().setColorFilter(color, PorterDuff.Mode.MULTIPLY);

        loginScreen.setVisibility(View.VISIBLE);
        loadingScreen.setVisibility(View.GONE);

        progressBar = (ProgressBar) findViewById(R.id.progress);
        progressBar.getProgressDrawable().setColorFilter(color, PorterDuff.Mode.SRC_IN);

        webView = (WebView) findViewById(R.id.webView);
        WebChromeClient webChromeClient = new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int progress) {
                if (progress < 100 && progressBar.getVisibility() == View.GONE) {
                    progressBar.setVisibility(ProgressBar.VISIBLE);
                }
                progressBar.setProgress(progress);
                if (progress == 100) {
                    progressBar.setVisibility(ProgressBar.GONE);
                }
            }
        };
        webView.setWebChromeClient(webChromeClient);
        webView.setWebViewClient(new LoginWebViewClient());

        WebSettings webSettings = webView.getSettings();
        webSettings.setBuiltInZoomControls(true);
        webSettings.setDisplayZoomControls(false);
        webSettings.setGeolocationEnabled(true);
        webSettings.setSupportMultipleWindows(true);
        webSettings.setJavaScriptEnabled(true);

        CookieManager.getInstance().removeSessionCookies(null);
        CookieManager.getInstance().removeAllCookies(null);
        CookieManager.getInstance().flush();



        if (LoginUtil.useStsLogin(this)) {
            Log.i(TAG, "doing sts login");
            stsLogin();
        } else {
            webLogin();
        }
    }

    void webLogin() {
        String authUrl = getString(R.string.config_web_login_url);
        Log.i(TAG, "loading login url=" + authUrl);
        webView.loadUrl(authUrl);
    }

    // If sts login is enabled, try the login flow.  if successful, setup the app
    private void stsLogin() {
        subscription.add(
                LoginUtil.stsLogin(this)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action1<Boolean>() {
                            @Override
                            public void call(Boolean success) {
                                if (success) {
                                    Log.v(TAG, "stsLogin completed successfully");
                                    completeLogin();
                                } else {
                                    webLogin();
                                    Toast.makeText(LoginActivity.this, "STS Authentication failed!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }, new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                throw new OnErrorNotImplementedException(TAG + ".stsLogin()", throwable);
                            }
                        })
        );
    }

    void completeLogin() {
        final Context context = getApplicationContext();
        PreferenceManager.setDefaultValues(context, R.xml.preferences, false);
        loginScreen.setVisibility(View.GONE);
        loadingScreen.setVisibility(View.VISIBLE);
        subscription.add(
                LoginUtil.onStartup(context)
                        .flatMap(new Func1<Boolean, Observable<Boolean>>() {
                            @Override
                            public Observable<Boolean> call(Boolean success) {
                                if (!success) {
                                    throw new RuntimeException("completeLogin: Login.onStartup() -> false");
                                }
                                return DeviceManager.updateUserDevice(context);
                            }
                        })
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action1<Boolean>() {
                            @Override
                            public void call(Boolean success) {
                                if (!success) {
                                    Log.e(TAG, "DeviceManager.updateUserDevice() -> false, won't get notifications!");
                                }
                                LoginUtil.startMainActivity(context);
                                finish();
                            }
                        }, new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                throw new OnErrorNotImplementedException("completeLogin", throwable);
                            }
                        })
        );
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((ViewGroup) webView.getParent()).removeView(webView);
        webView.removeAllViews();
        webView.destroy();

        subscription.clear();
    }

    @Override
    public void onPause() {
        super.onPause();
        webView.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        webView.onResume();
        loginScreen.setVisibility(View.VISIBLE);
        loadingScreen.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class LoginWebViewClient extends WebViewClient {
        private String urlWithUsername;

        // Overriding overloaded function because the String url version is deprecated.
        // However, if the String version is not overridden, this doesn't work.
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            String url = request.getUrl().toString();
            return shouldOverrideUrlLoading(view, url);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            urlWithUsername = url;

            CookieManager cookieManager = CookieManager.getInstance();
            String cookies = cookieManager.getCookie(url);
            if (cookies == null) {
                return false;
            }

            String[] cookieArray = cookies.split(";");
            String rtfa = null;
            String fedAuth = null;

            for (String cookie : cookieArray) {
                if (cookie.contains("rtFa")) {
                    rtfa = cookie;
                } else if (cookie.contains("FedAuth")) {
                    fedAuth = cookie;
                }
            }

            if (rtfa != null && fedAuth != null) {
                Context context = getApplicationContext();
                UserPrefs userPrefs = UserPrefs.getInstance(context);
                userPrefs.setCookie(rtfa, fedAuth);
                String cookie = userPrefs.getCookie();

                // Sync point between legacy SharedPrefStorage and new UserPrefs
                SharedPrefStorage.savePreference(context, SharedPrefStorage.ANDROID_COOKIE_HEADER, cookie);

                Log.v(TAG, "logging in via shouldOverrideUrlLoading() cookie=" + cookie);
                completeLogin();
                return true;
            }
            return false;
        }

        // This method is used with tmo's vpn
        @Override
        public void onReceivedHttpAuthRequest(WebView view, HttpAuthHandler handler, String host, final String realm) {
            Log.v(TAG, "onReceivedHttpAuthRequest called");

            Pattern p = Pattern.compile("username=([^\\&]*)");
            Matcher m = p.matcher(urlWithUsername);

            String username = "";
            if (m.find()) {
                username = m.group(1).replace("%40", "@");
            }
            loginDialog(username, handler);
        }
    }

    // This method is used with tmo's vpn
    void loginDialog(String username, final HttpAuthHandler handler) {
        View dialog = getLayoutInflater().inflate(R.layout.dialog_login, null);
        final EditText usernameInput = (EditText) dialog.findViewById(R.id.username);
        final EditText passwordInput = (EditText) dialog.findViewById(R.id.password);
        usernameInput.setText(username);
        passwordInput.requestFocus();

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.Tmobile_Dialog);
        builder.setView(dialog)
                .setTitle(R.string.authentication_required)
                .setPositiveButton(R.string.log_in, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Log.v(TAG, "user login via HttpAuthHandler");
                        handler.proceed(usernameInput.getText().toString(), passwordInput.getText().toString());
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Log.v(TAG, "user cancel");
                        handler.cancel();
                        webLogin();
                    }
                })
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        Log.v(TAG, "canceled login");
                        handler.cancel();
                        webLogin();
                    }
                })
                .show();
    }
}