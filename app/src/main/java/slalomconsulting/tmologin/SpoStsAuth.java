package slalomconsulting.tmologin;

import android.content.Context;
import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicReference;

import javax.xml.parsers.ParserConfigurationException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicHeader;
import cz.msebera.android.httpclient.protocol.HTTP;

/**
 * Created by joeyt on 8/21/16.
 */
public abstract class SpoStsAuth {
    static final String TAG = SpoStsAuth.class.getSimpleName();

    private static String buildSTSTokenMessage(String siteUrl, String username, String password) {
        return "<s:Envelope xmlns:s=\"http://www.w3.org/2003/05/soap-envelope\"\n" +
                "      xmlns:a=\"http://www.w3.org/2005/08/addressing\"\n" +
                "      xmlns:u=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">\n" +
                "  <s:Header>\n" +
                "    <a:Action s:mustUnderstand=\"1\">http://schemas.xmlsoap.org/ws/2005/02/trust/RST/Issue</a:Action>\n" +
                "    <a:ReplyTo>\n" +
                "      <a:Address>http://www.w3.org/2005/08/addressing/anonymous</a:Address>\n" +
                "    </a:ReplyTo>\n" +
                "    <a:To s:mustUnderstand=\"1\">https://login.microsoftonline.com/extSTS.srf</a:To>\n" +
                "    <o:Security s:mustUnderstand=\"1\"\n" +
                "       xmlns:o=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">\n" +
                "      <o:UsernameToken>\n" +
                "        <o:Username>" + username + "</o:Username>\n" +
                "        <o:Password>" + password + "</o:Password>\n" +
                "      </o:UsernameToken>\n" +
                "    </o:Security>\n" +
                "  </s:Header>\n" +
                "  <s:Body>\n" +
                "    <t:RequestSecurityToken xmlns:t=\"http://schemas.xmlsoap.org/ws/2005/02/trust\">\n" +
                "      <wsp:AppliesTo xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\">\n" +
                "        <a:EndpointReference>\n" +
                "          <a:Address>" + siteUrl + "</a:Address>\n" +
                "        </a:EndpointReference>\n" +
                "      </wsp:AppliesTo>\n" +
                "      <t:KeyType>http://schemas.xmlsoap.org/ws/2005/05/identity/NoProofKey</t:KeyType>\n" +
                "      <t:RequestType>http://schemas.xmlsoap.org/ws/2005/02/trust/Issue</t:RequestType>\n" +
                "      <t:TokenType>urn:oasis:names:tc:SAML:1.0:assertion</t:TokenType>\n" +
                "    </t:RequestSecurityToken>\n" +
                "  </s:Body>\n" +
                "</s:Envelope>";
    }

    public static String getSecurityToken(final Context context, String username, String password) {
        String stsMessage = buildSTSTokenMessage(context.getString(R.string.config_sitecollection_url), username, password);

        SyncHttpClient client = new SyncHttpClient();
        StringEntity stsEntity = new StringEntity(stsMessage, "UTF-8");
        client.setTimeout(10 * 1000);  // 10 seconds
        final AtomicReference<String> token = new AtomicReference<>();
        final String url = context.getString(R.string.config_sts_url);
        client.post(context, url, stsEntity, "application/x-www-form-urlencoded", new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.e(TAG, "getSecurityToken.onFailure url=" + url + " statusCode=" + statusCode + " response=" + responseString, throwable);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String response) {
                XmlParser parser;
                try {
                    parser = new XmlParser(response);
                } catch (SAXException | ParserConfigurationException | IOException e) {
                    Log.e(TAG, "getSecurityToken.onSuccess instantiating XmlParser response=" + response, e);
                    return;
                }
                final String securityToken;
                try {
                    securityToken = parser.getElementStringByTagName("wsse:BinarySecurityToken");
                } catch (SAXException | ParserConfigurationException | IOException e) {
                    Log.e(TAG, "getSecurityToken.onSuccess parser.getElementStringByTagName response=" + response, e);
                    return;
                }
                if (securityToken == null || securityToken.equals("")) {
                    Log.e(TAG, "getSecurityToken.onSuccess - Security Token was not found.");
                    return;
                }

                token.set(securityToken);
            }
        });
        return token.get();
    }

    public static String login(final Context context, String securityToken) {
        StringEntity accessTokenEntity = new StringEntity(securityToken, "UTF-8");
        SyncHttpClient client = new SyncHttpClient();
        client.setTimeout(10 * 1000);
        final AtomicReference<String> cookie = new AtomicReference<>();
        final String url = context.getString(R.string.config_sts_login_url);
        client.post(context, url, accessTokenEntity, "application/x-www-form-urlencoded", new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                if (statusCode == 302) {
                    String parsedCookie = parseCookie(context, headers);
                    if (parsedCookie == null) {
                        Log.e(TAG, "login.onFailure 302 no cookies response=" + responseString, throwable);
                    } else {
                        cookie.set(parsedCookie);
                    }
                } else {
                    Log.e(TAG, "login.onFailure url=" + url + " statusCode=" + statusCode + " response=" + responseString, throwable);
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                String parsedCookie = parseCookie(context, headers);
                if (parsedCookie == null) {
                    Log.e(TAG, "login.onSuccess no cookies response=" + responseString);
                } else {
                    cookie.set(parsedCookie);
                }
            }
        });
        return cookie.get();
    }

    static String parseCookie(Context context, Header[] headers) {
        String rtfa = null;
        String fedAuth = null;
        for (Header header : headers) {
            String headerName = header.getName();
            if ("Set-Cookie".equals(headerName)) {
                String headerValue = header.getValue();
                if (headerValue.startsWith("rtFa=")) {
                    rtfa = headerValue;
                } else if (headerValue.startsWith("FedAuth=")) {
                    fedAuth = headerValue;
                }
            }
        }

        if (rtfa != null && fedAuth != null) {
            UserPrefs userPrefs = UserPrefs.getInstance(context);
            userPrefs.setCookie(rtfa, fedAuth);
            String cookie = userPrefs.getCookie();
            Log.v(TAG, "parseCookie got=" + cookie);
            return cookie;
        }

        return null;
    }

    public static String getFormDigestValue(Context context, String cookie) {
        SyncHttpClient client = new SyncHttpClient();
        client.setTimeout(10 * 1000);
        String contentType = "application/json;odata=verbose;";
        client.addHeader("Accept", contentType);
        client.addHeader("Cookie", cookie);
        StringEntity entity = new StringEntity("", "UTF-8");
        entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, contentType));
        final AtomicReference<String> formDigest = new AtomicReference<>();
        final String url = context.getString(R.string.config_sitecollection_url) + "/_api/contextinfo";
        client.post(context, url, entity, contentType, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String formDigestValue;
                try {
                    JSONObject d = response.getJSONObject("d");
                    JSONObject getContextWebInformation = d.getJSONObject("GetContextWebInformation");
                    formDigestValue = getContextWebInformation.getString("FormDigestValue");
                } catch (JSONException e) {
                    Log.e(TAG, "getFormDigestValue json url=" + url, e);
                    return;
                }
                formDigest.set(formDigestValue);
            }
        });
        return formDigest.get();
    }

}
