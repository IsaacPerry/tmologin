package slalomconsulting.tmologin;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

/*
 * Singleton for UserPrefs.  These values persist across multiple app launches, until the user logs out.
 * Obviously backed by a SharedPreferences file, but that should be transparent to callers.
 */
public class UserPrefs {

    private static UserPrefs instance = null;

    public synchronized static UserPrefs getInstance(Context context) {
        if (instance == null) {
            instance = new UserPrefs(context.getApplicationContext());
        }
        return instance;
    }

    private static final String PREF_NAME = "tnation.user";
    private static final String KEY_COOKIE = "tnation.user.spo.cookie";
    private static final String KEY_RTFA = "tnation.user.spo.rtfa";
    private static final String KEY_FEDAUTH = "tnation.user.spo.fedauth";
    private static final String KEY_DISPLAY_NAME = "tnation.user.display.name";
    private static final String KEY_SPO_GUID = "tnation.user.spo.guid";
    private static final String KEY_ONE_TEAM = "tnation.user.spo.one.team";
    private static final String KEY_EXEMPT = "tnation.user.exempt";
    private static final String KEY_LAST_NONEXEMPT_MILLIS = "tnation.app.last.nonexempt.dialog";
    private static final String KEY_AVAILABLE = "tnation.user.available";
    private static final String KEY_IMAGE_URL = "tnation.user.image.url";

    private final SharedPreferences prefs;

    private UserPrefs(Context c) {
        this.prefs = c.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public void setCookie(String rtfa, String fedAuth) {
        savePref(KEY_RTFA, rtfa);
        savePref(KEY_FEDAUTH, fedAuth);
        String cookie = String.format("%s;%s", rtfa, fedAuth);
        savePref(KEY_COOKIE, cookie);
    }

    private void savePref(String key, String value) {
        prefs.edit().putString(key, value).apply();
    }

    private void savePref(String key, boolean value) {
        prefs.edit().putBoolean(key, value).apply();
    }

    private void savePref(String key, long value) {
        prefs.edit().putLong(key, value).apply();
    }

    void reset() {
        prefs.edit().clear().apply();
    }

    @NonNull
    public String getCookie() {
        return prefs.getString(KEY_COOKIE, "");
    }

    @NonNull
    public String getDisplayName() {
        return prefs.getString(KEY_DISPLAY_NAME, "");
    }

    public void setDisplayName(@NonNull String displayName) {
        savePref(KEY_DISPLAY_NAME, displayName);
    }

    // Note, you may expect this value to be UPPERCASE
    @NonNull
    public String getSpoGuid() {
        return prefs.getString(KEY_SPO_GUID, "");
    }

    // Note, please save this String as UPPERCASE
    void setSpoGuid(@NonNull String spoGuid) {
        savePref(KEY_SPO_GUID, spoGuid);
    }

    @NonNull
    public String getRtfa() {
        return prefs.getString(KEY_RTFA, "");
    }

    @NonNull
    public String getFedauth() {
        return prefs.getString(KEY_FEDAUTH, "");
    }

    void setOneTeam(boolean enabled) {
        savePref(KEY_ONE_TEAM, enabled);
    }

    public boolean isOneTeam() {
        return prefs.getBoolean(KEY_ONE_TEAM, false);
    }

    boolean isLoggedIn() {
        return !"".equals(getCookie());
    }

    public boolean isExempt() {
        return prefs.getBoolean(KEY_EXEMPT, false);
    }

    public void setExempt(boolean exempt) {
        savePref(KEY_EXEMPT, exempt);
    }

    public long getLastNonexemptMillis() {
        return prefs.getLong(KEY_LAST_NONEXEMPT_MILLIS, 0);
    }

    public void setLastNonexemptMillis(long millis) {
        savePref(KEY_LAST_NONEXEMPT_MILLIS, millis);
    }

    public boolean isAvailable() {
        return prefs.getBoolean(KEY_AVAILABLE, false);
    }

    public void setAvailable(boolean available) {
        savePref(KEY_AVAILABLE, available);
    }

    @Nullable
    public String getImageUrl() {
        return prefs.getString(KEY_IMAGE_URL, null);
    }

    public void setImageUrl(String url) {
        if (!TextUtils.isEmpty(url)) {
            savePref(KEY_IMAGE_URL, url);
        }
    }
}
