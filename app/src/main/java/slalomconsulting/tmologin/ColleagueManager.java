package slalomconsulting.tmologin;

import android.content.Context;
import android.support.v4.util.SimpleArrayMap;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import rx.Observable;

/**
 * Replacing JDeferred with RX as that is where the industry (and therefore StackOverflow) is going.
 * <p/>
 * This Presenter class abstracts the Model sources from the View.  This way the View doesn't care if
 * data is coming from Sharepoint or Sqlite (and importantly doesn't know details about either).
 */
public abstract class ColleagueManager {
    // private constructor to prevent instantiation
    private ColleagueManager() {
    }

    static final String TAG = ColleagueManager.class.getSimpleName();


    public static Observable<Colleague> findColleagueMe(final Context context) {
        return Observable.fromCallable(new Callable<Colleague>() {
            @Override
            public Colleague call() throws Exception {
                return SpoColleague.findColleagueMe(context);
            }
        });
    }
}
