package slalomconsulting.tmologin;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicReference;

import cz.msebera.android.httpclient.Header;

/**
 * Replacing JDeferred with RX as that is where the industry (and therefore StackOverflow) is going.
 */
public abstract class SpoColleague {
    private SpoColleague() {
        // private constructor to prevent instantiation
    }

    static final String TAG = SpoColleague.class.getSimpleName();


    static Colleague parseColleague(JSONObject response) throws JSONException {
        JSONArray results = response.getJSONObject("d").getJSONObject("UserProfileProperties").getJSONArray("results");
        return parseColleague(results);
    }

    @NonNull
    public static Colleague parseColleague(JSONArray results) throws JSONException {
        Colleague person = new Colleague();
        for (int i = 0; i < results.length(); i++) {
            JSONObject result = (JSONObject) results.get(i);
            String key = result.getString("Key");
            if (!result.isNull("Value")) {
                String value = result.getString("Value");
                if ("".equals(value)) continue;

                switch (key) {
                    case SpoJsonKeys.WORK_EMAIL:
                        person.setWorkEmail(value);
                        break;
                    case SpoJsonKeys.WORK_PHONE:
                        person.setWorkPhone(value);
                        break;
                    case SpoJsonKeys.CELL_PHONE:
                        person.setCellPhone(value);
                        break;
                    case SpoJsonKeys.USER_PROFILE_GUID:
                        person.setUserProfileGuid(value.toUpperCase());
                        break;
                    case SpoJsonKeys.OFFICE:
                        person.setOffice(value);
                        break;
                    case SpoJsonKeys.FIRST_NAME:
                        person.setFirstName(value);
                        break;
                    case SpoJsonKeys.LAST_NAME:
                        person.setLastName(value);
                        break;
                    case SpoJsonKeys.ACCOUNT_NAME:
                        person.setAccountName(value);
                        break;
                    case SpoJsonKeys.PICTURE_URL:
                    case SpoJsonKeys.PICTURE_URL2: // <3 sharepoint
                        person.setPictureUrl(value);
                        break;
                    case SpoJsonKeys.JOB_TITLE:
                        person.setJobTitle(value);
                        break;
                    case SpoJsonKeys.OFFICE_NUMBER:
                        person.setOfficeNumber(value);
                        break;
//                    default:
//                        Log.v(TAG, "parseColleague: unhandled key=" + key + " value=" + value);
//                        break;
                }
            }
        }
        return person;
    }

    // TODO This function should replace SpoUserProfile.getMyUserProfileProperties
    public static Colleague findColleagueMe(final Context context) {
        SyncHttpClient client = SpoUtil.getHttpClient(context);
        String url = context.getString(R.string.config_sitecollection_url) + "/_api/SP.UserProfiles.PeopleManager/GetMyProperties";
        Log.v(TAG, "findColleagueMe: url=" + url);
        final AtomicReference<Colleague> reference = new AtomicReference<>();
        client.get(context, url, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.v(TAG, "findColleagueMe: sc=" + statusCode);
                Colleague person;
                try {
                    person = parseColleague(response);
                } catch (JSONException e) {
                    throw new RuntimeException(TAG + ".findColleagueMe.onSuccess json", e);
                }
                reference.set(person);
            }
        });

        return reference.get();
    }
}
