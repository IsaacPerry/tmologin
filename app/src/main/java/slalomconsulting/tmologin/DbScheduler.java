package slalomconsulting.tmologin;

import java.util.concurrent.Executors;

import rx.Scheduler;
import rx.schedulers.Schedulers;

/**
 * Created by isaac.charny on 8/9/16.
 */
public abstract class DbScheduler {
    public static Scheduler getInstance() {
        return sharedScheduler;
    }

    private static Scheduler sharedScheduler = Schedulers.from(Executors.newSingleThreadExecutor());
}
