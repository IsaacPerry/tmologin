package slalomconsulting.tmologin;

import android.content.Context;
import android.content.Intent;
import android.util.Log;


import java.util.concurrent.Callable;

import rx.Observable;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Helper functions for login-related functionality.
 */
public abstract class LoginUtil {
    private LoginUtil() {
        // private constructor to prevent instantiation
    }

    static final String TAG = LoginUtil.class.getSimpleName();

    static boolean useStsLogin(Context context) {
        return false;
    }

    public static void startMainActivity(Context context) {
        Intent main = new Intent(context, MainActivity.class);
        main.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(main);
    }

    static Observable<Boolean> stsLogin(final Context context) {
        return Observable.fromCallable(new Callable<String>() {
            @Override
            public String call() throws Exception {
                String username = context.getString(R.string.config_sts_username);
                String password = context.getString(R.string.config_sts_password);
                return SpoStsAuth.getSecurityToken(context, username, password);
            }
        })
                .flatMap(new Func1<String, Observable<String>>() {
                    @Override
                    public Observable<String> call(final String s) {
                        if (s == null || "".equals(s)) {
                            Log.w(TAG, "stsLoginSecurityToken failed");
                            return Observable.just("");
                        }
                        return Observable.fromCallable(new Callable<String>() {
                            @Override
                            public String call() throws Exception {
                                return SpoStsAuth.login(context, s);
                            }
                        });
                    }
                })
                .flatMap(new Func1<String, Observable<String>>() {
                    @Override
                    public Observable<String> call(final String s) {
                        if (s == null || "".equals(s)) {
                            Log.w(TAG, "stsLogin failed");
                            return Observable.just("");
                        }
                        // Sync point between legacy SharedPrefStorage
                        SharedPrefStorage.savePreference(context, SharedPrefStorage.ANDROID_COOKIE_HEADER, s);
                        return Observable.fromCallable(new Callable<String>() {
                            @Override
                            public String call() throws Exception {
                                return SpoStsAuth.getFormDigestValue(context, s);
                            }
                        });
                    }
                })
                .map(new Func1<String, Boolean>() {
                    @Override
                    public Boolean call(String s) {
                        if (s == null || "".equals(s)) {
                            Log.w(TAG, "stsGetFormDigest failed");
                            return false;
                        } else {
                            return true;
                        }
                    }
                });

    }

    /**
     * On app startup, query the service for user information.  The service may reject this request
     * if not properly authenticated, so this is also a confirmation if the user is logged in.
     *
     * Additionally, this opens the database.  The first time the db is opened it may be slow, so
     * later db calls will be faster.
     */
    public static Observable<Boolean> onStartup(final Context context) {
        return ColleagueManager.findColleagueMe(context)
                .subscribeOn(Schedulers.io())
                .observeOn(DbScheduler.getInstance())
                .map(new Func1<Colleague, Boolean>() {
                    @Override
                    public Boolean call(Colleague colleague) {
                        if (colleague == null) {
                            Log.w(TAG, "onStartup: returned null colleague");
                            return false;
                        }
                        ColleagueDao.getInstance(context).replaceMe(colleague);
                        String guid = colleague.getUserProfileGuid();
                        final UserPrefs userPrefs = UserPrefs.getInstance(context);
                        userPrefs.setSpoGuid(guid.toUpperCase());
                        userPrefs.setDisplayName(colleague.getDisplayName());
                        userPrefs.setImageUrl(colleague.getPictureUrl());
                        Log.v(TAG, "onStartup: guid=" + guid);
                        return true;
                    }
                });
    }
}
