package slalomconsulting.tmologin;

/**
 * Created by joeyt on 8/17/16.
 */
interface SpoJsonKeys {
    String ACCOUNT_NAME = "AccountName";
    String PICTURE_URL = "PictureURL";
    String PICTURE_URL2 = "PictureUrl";
    String USER_PROFILE_GUID = "UserProfile_GUID";
    String TITLE = "Title";
    String LAST_NAME = "LastName";
    String FIRST_NAME = "FirstName";
    String OFFICE = "Office";
    String OFFICE_NUMBER = "OfficeNumber";
    String JOB_TITLE = "JobTitle";
    String CELL_PHONE = "CellPhone";
    String WORK_PHONE = "WorkPhone";
    String WORK_EMAIL = "WorkEmail";
}
