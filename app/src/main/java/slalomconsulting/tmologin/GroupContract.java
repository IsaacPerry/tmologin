package slalomconsulting.tmologin;

import android.provider.BaseColumns;


/**
 * Created by isaac.charny on 8/18/16.
 */
interface GroupContract {
    String TABLE_NAME = "groups";
    String SQL_CREATE_TABLE = "create table " +
            TABLE_NAME + " ( " +
            Cols._ID + " integer primary key not null, " +
            Cols.GROUP_ID + " integer not null, " +
            Cols.TITLE + " text not null, " +
            Cols.PATH + " text unique not null, " +
            Cols.TYPE_ENUM + " integer not null default 0, " +
            Cols.VISIBILITY + " text not null default '" + Group.Visibility.PRIVATE.dbVal + "', "+
            Cols.SUBSCRIBED + " integer not null default " + DbHelper.SQLITE_FALSE + ", " +
            "unique (" + Cols.GROUP_ID + ", " + Cols.VISIBILITY + ") " +
            "); ";
    String SQL_DROP_TABLE = "drop table if exists " + TABLE_NAME;


    interface Cols extends BaseColumns {
        String GROUP_ID = "group_id";
        String TITLE = "title";
        String PATH = "path";
        String VISIBILITY = "visibility";
        String SUBSCRIBED = "subscribed";
        String TYPE_ENUM = "type_enum";
    }
}

