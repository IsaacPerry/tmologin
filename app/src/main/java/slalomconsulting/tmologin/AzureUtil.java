package slalomconsulting.tmologin;

import android.content.Context;

import com.loopj.android.http.SyncHttpClient;

/**
 * Common functions specific to our Azure implementation.
 */
abstract class AzureUtil {
    private AzureUtil() {
        // private constructor to prevent instantiation
    }

    static final String CONTENT_TYPE = "application/json;charset=utf-8";

    static SyncHttpClient getHttpClient(Context context) {
        SyncHttpClient client = new SyncHttpClient();
        client.setTimeout(10 * 1000);  // 10 seconds
        client.addHeader("Accept", CONTENT_TYPE);
        client.addHeader("Cookie", UserPrefs.getInstance(context).getCookie());
        return client;
    }
}
